package main;

import java.io.*;
import java.util.*;
import java.util.function.Predicate;

/**
 * Created with IntelliJ IDEA.
 * User: san
 * Date: 10/12/13
 * Time: 7:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class Generator {

    static int maxPossibleLightWeight;

    public static class Fuel {
        String code;
        String name;
        double mfull;
        double mempty;
        boolean large;

        public Fuel(String code, String name, double mfull, double mempty, boolean large) {
            this.code = code;
            this.name = name;
            this.mfull = mfull;
            this.mempty = mempty;
            this.large = large;
        }
    }

    public static class Decoupler {
        String code;
        String name;
        double mass;

        public Decoupler(String code, String name, double mass) {
            this.code = code;
            this.name = name;
            this.mass = mass;
        }
    }

    public static class Engine {
        String code;
        String name;
        boolean large;
        double mass;
        double thrust;
        double isp_atm;
        double isp;

        public Engine(String code, String name, boolean large, double mass, double thrust, double isp_atm, double isp) {
            this.code = code;
            this.name = name;
            this.large = large;
            this.mass = mass;
            this.thrust = thrust;
            this.isp_atm = isp_atm;
            this.isp = isp;
        }

    }

    static Fuel zeroFuel = new Fuel("ZERO", "Zero Fuel", 0, 0, false);

    public static Fuel[] fuels = new Fuel[] {

            new Fuel("FLT100", "FL-T100 Fuel Tank", 0.5625, 0.0625, false),
            new Fuel("FLT200", "FL-T200 Fuel Tank", 1.125, 0.125, false),
            new Fuel("FLT400", "FL-T400 Fuel Tank", 2.25,0.25, false),
            new Fuel("FLT800", "FL-T800 Fuel Tank", 4.5,0.5, false),

            new Fuel("X2008", "Rockomax X200-8 Fuel Tank",  4.5,0.5, true),
            new Fuel("X20016", "Rockomax X200-16 Fuel Tank", 9, 1, true),
            new Fuel("X20032", "Rockomax X200-32 Fuel Tank", 18,2, true),
            new Fuel("JUMBO64", "Rockomax Jumbo-64 Fuel Tank", 36,4, true),
    };

    public static Fuel getFuel(String code) {
        for (Fuel fuel : fuels) {
            if (fuel.code.equals(code)) return fuel;
        }
        return null;
    }

    static Engine zeroEngine = new Engine("ZERO", "Zero Engine", false, 0, 0, 0, 0);
    public static Engine[] engines = new Engine[] {
            new Engine("LVT30", "LV-T30 Liquid Fuel Engine", false, 1.25, 215, 320, 370),
//            new Engine("LVT45", "LV-T45 Liquid Fuel Engine", false, 1.5, 200,320,270),
            new Engine("LV909", "LV-909 Liquid Fuel Engine", false, 0.5, 50,300,390),
            new Engine("TOR", "Toroidal Aerospike Rocket", false, 1.5, 175,388,390),
            new Engine("POODLE", "Rockomax \"Poodle\" Liquid Engine", true, 2.5, 220,270,390),
            new Engine("MAINSAIL", "Rockomax \"Mainsail\" Liquid Engine", true, 6, 1500,280, 330),
            new Engine("SKIPPER", "Rockomax \"Skipper\" Liquid Engine", true, 4, 650,300,350),
            new Engine("LVN", "LV-N Atomic Rocket Engine", false, 2.25, 60,220, 800),
    };

    public static int[] sides = new int[] {0,2,3,4,6};

    public static Engine getEngine(String code) {
        for (Engine engine : engines) {
            if (engine.code.equals(code)) return engine;
        }
        return null;
    }

    public static Decoupler[] decouplers = new Decoupler[] {
            new Decoupler("LL","Rockomax Brand Decoupler", 0.4),
            new Decoupler("NN","TR-18A Stack Decoupler", 0.05),
    };

    public static class Replacement1To2 implements Comparable<Replacement1To2> {
        Stage stage1;
        Stage stage2;
        double score;

        public Replacement1To2(Stage stage1, Stage stage2) {
            this.stage1 = stage1;
            this.stage2 = stage2;
        }

        @Override
        public int compareTo(Replacement1To2 o) {
            return (int)Math.signum(score - o.score);
        }
    }

    public static class Stage {
        Fuel fuel;
        Engine engine;
        boolean central;
        int numSideParts;
        int numEngines;
        int height;

        //// thrust to weight ratio
        double twr;

        //  dv it can add alone
        double freedv;
        double freedvPerMass;

        double ownMass;

        TreeSet<Replacement1To2> better = new TreeSet<>();
        TreeSet<Replacement1To2> worse = new TreeSet<>();


        Stage(Fuel fuel, Engine engine, boolean central, int numSideParts, int numEngines, int height) {
            this.fuel = fuel;
            this.engine = engine;
            this.central = central;
            this.numSideParts = numSideParts;
            this.numEngines = numEngines;
            this.height = height;

            ownMass = _getFullMass();
            twr = numEngines * engine.thrust / getFullMass();
        }

        public double getDV(int massAcc, boolean atm) {  // including decoupler
            final double massOfEngines = numEngines * engine.mass;
            final int nfueltanks = height * ((central ? 1 : 0) + numSideParts);
            final double m_start = massAcc + massOfEngines + nfueltanks * fuel.mfull;
            final double m_end = massAcc + massOfEngines + nfueltanks * fuel.mempty;
            final double isp = atm? engine.isp_atm : engine.isp;
            final double dv = Math.log(m_start/m_end) * isp * 9.816;
            return dv;
        }

        public double _getFullMass() {
            final double massOfEngines = numEngines * engine.mass;
            final int nfueltanks = height * ((central ? 1 : 0) + numSideParts);
            return massOfEngines + nfueltanks * fuel.mfull;
        }

        public double getFullMass() {
            return ownMass;
        }

        public String toJSON() {
            return String.format("     {       \"fuel\": \"%s\",\n" +
                    "            \"engine\":\"%s\",\n" +
                    "            \"central\": %s,\n" +
                    "            \"numSideParts\": %d,\n" +
                    "            \"numEngines\": %d,\n" +
                    "            \"height\": %d}\n",
                    fuel.name,
                    engine.name.replace("\"","\\\""),
                    String.valueOf(central),
                    numSideParts, numEngines, height
                    );


        }

        public boolean canAtmosphere(double extraMass) {
            return numEngines * engine.thrust / (getFullMass() + extraMass) >= 15;
        }

        public boolean canVacuum(double extraMass) {
            return numEngines * engine.thrust / (getFullMass() + extraMass) >= 5;
        }

        @Override
        public String toString() {
            return "[Stage: ENG:"+engine.code+" FUE:"+fuel.code+" MASS:"+getFullMass()+"]";
        }
    }

    public static class Configuration {
        Stage[] stages;
        int nstages;

        double mass;

        Configuration(Stage[] stages, int nstages) {
            this.stages = stages;
            this.nstages = nstages;
        }

        Configuration(Configuration hi, Configuration lo) {
            nstages = hi.nstages + lo.nstages;
            stages = new Stage[nstages];
            for(int i=0; i<hi.nstages; i++) {
                stages[i] = hi.stages[i];
            }
            for(int i=0; i<lo.nstages; i++) {
                stages[hi.nstages + i] = lo.stages[i];
            }
        }

        public int getHeight() {
            int h = 0;
            for (Stage stage : stages) {
                h += stage.height;
            }
            return h;
        }

        public String toJSON() {
            StringBuffer sb = new StringBuffer("[");
            for (int i = 0; i < nstages; i++) {
                Stage stage = stages[i];
                if (i != 0) sb.append(",");
                sb.append(stage.toJSON());
            }
            sb.append("]");
            return sb.toString();
        }

        public Configuration cloneIt() {
            Stage[] newstages = new Stage[stages.length];
            System.arraycopy(stages, 0, newstages, 0, stages.length);
            Configuration conf = new Configuration(newstages, nstages);
            conf.mass = mass;
            return conf;
        }

        public String toJSONStringAndInfo(double requiredm) {
            StringBuilder sb = new StringBuilder();
            for (Stage stage : stages) {
                if (sb.length() == 0) {
                    sb.append("[");
                } else {
                    sb.append(",");
                }
                sb.append(stage.toJSON());
            }
            sb.append("]");
            sb.insert(0, "Mass="+calculateMass(this, requiredm)+" ");
            return sb.toString();
        }
    }

    static class Buf {
        double[] accels = new double[7];
        double[] dvs = new double[7];
    }

    static class BaseResult {
        double remainingDV;
        double remainingMass;
    }

    public static class HeavyConfiguration implements Serializable {

        private static final long serialVersionUID = 1;

        Stage i;
        Stage j;
        Stage k;
        Stage l;
        private double usedMV;
        double availableMass;
        double usedMassEst;


        public HeavyConfiguration(Stage i, Stage j, double usedMV, double usedMassEst) {
            this.i = i;
            this.j = j;
            this.usedMV = usedMV;
            this.usedMassEst = usedMassEst;
        }
    }


    static ArrayList<HeavyConfiguration> calculateHeavyConfigs(double lightMass) {

        heavyStages = new ArrayList(Arrays.asList(possibleStages.stream().filter((x) ->
                x.canAtmosphere(lightMass)).toArray()));
        Collections.sort(heavyStages, new Comparator<Stage>() {
            @Override
            public int compare(Stage o1, Stage o2) {
                return (int)Math.signum(o1.getFullMass() - o2.getFullMass());
            }
        });
        ArrayList<HeavyConfiguration> heavyConfigurations = new ArrayList<>(400000);
        // System.out.println("Heavy stages: "+heavyStages.size());
        Configuration testconf = new Configuration(new Stage[2], 2);
        Buf buf = new Buf();
        for(int i=0; i<heavyStages.size(); i++) {
            testconf.stages[0] = heavyStages.get(i);
            for(int j=0; j<heavyStages.size(); j++) {
                testconf.stages[1] = heavyStages.get(j);
                double usedMV = verifyAnySolution(testconf, lightMass, buf);
                if (usedMV >= 5000) {
                    heavyConfigurations.add(new HeavyConfiguration(testconf.stages[0], testconf.stages[1], usedMV, testconf.stages[0].getFullMass() + testconf.stages[1].getFullMass()));
                }
            }
        }
//            System.out.println("Raw heavy configs: "+ heavyConfigurations.size());
//            ArrayList<HeavyConfiguration> newHeavyConfigurations = new ArrayList<>();
//            for (Iterator<HeavyConfiguration> iterator = heavyConfigurations.iterator(); iterator.hasNext(); ) {
//                HeavyConfiguration heavyConfiguration = iterator.next();
//                testconf.stages[0] = heavyConfiguration.i;
//                testconf.stages[1] = heavyConfiguration.j;
//                for (int m = 0; m < 1000; m += 10) {
//                    boolean ok = verifyHeavyAccel(testconf, m, buf);
//                    if (ok) {
//                        // ok, keep going
//                    } else {
//                        if (m > 0) {
//                            for(int q=0; q<10; q++) {
//                                ok = verifyHeavyAccel(testconf, m - q, buf);
//                                if (ok) {
//                                    heavyConfiguration.availableMass = m - q;
//                                    break;
//                                }
//                            }
//                        }
//                        break;
//                    }
//                }
//                if (heavyConfiguration.usedMassEst < 160) {
//                    newHeavyConfigurations.add(heavyConfiguration);
//                }
//            }
//            heavyConfigurations = newHeavyConfigurations;
//            System.out.println("Found BASE configurations: "+ heavyConfigurations.size());
//            Collections.sort(heavyConfigurations, new Comparator<HeavyConfiguration>() {
//                @Override
//                public int compare(HeavyConfiguration o1, HeavyConfiguration o2) {
//                    return (int)Math.signum(o2.availableMass - o1.availableMass);
//                }
//
//            });
        return heavyConfigurations;
    }

    private static void findSolution(double startM, int dv) {
        Stage[] sta = new Stage[3];
        Buf buf = new Buf();
        int cnt = 0;
        Configuration conf = new Configuration(sta, sta.length);
        Random random = new Random(0);
        while(true) {
            sta[0] = possibleStages.get(random.nextInt(possibleStages.size()));
            sta[1] = possibleStages.get(random.nextInt(possibleStages.size()));
            sta[2] = possibleStages.get(random.nextInt(possibleStages.size()));
            double dvFound = verifyAnySolution(conf, startM, buf);
            if (dvFound >= dv) {
                System.out.println("OK!");
            } else {
                if (dvFound != 0) {
                    System.out.println("Something..");
                }
            }
            if (cnt % 100000 == 0) {
                System.out.println("cnt="+cnt);
            }
            cnt++;
        }
    }

    static double verifyAnySolution(Configuration configuration, double usefulMass, Buf buf) {
        double dvAcc = 0;
        double massAcc = usefulMass;
        final Stage[] stages = configuration.stages;
        int height = 0;
        final int nstages = configuration.nstages;
        for (int i = 0; i < nstages; i++) {
            final Stage stage = stages[i];
            final double decoupler = i > 0 ? ((stages[i-1].engine.large && stage.fuel.large) ? decouplers[0].mass : decouplers[1].mass) : 0;
            final double massOfEngines = stage.numEngines * stage.engine.mass;
            final int nfueltanks = stage.height * ((stage.central ? 1 : 0) + stage.numSideParts);
            final double m_start = massAcc + decoupler + massOfEngines + nfueltanks * stage.fuel.mfull;
            final double accel = stage.numEngines * stage.engine.thrust / m_start;
            if (i == nstages - 1 && accel < 15) return 0; // early check
            final double m_end = massAcc + decoupler + massOfEngines + nfueltanks * stage.fuel.mempty;
            final double isp = (i == nstages - 1)? stage.engine.isp_atm : stage.engine.isp;
            final double dv = Math.log(m_start/m_end) * isp * 9.816;
            height += stage.height;
            if (i == nstages - 1) {
                double remdv = 5000 - dv;
                int scan = i-1;
                while(remdv > 0) {
                    if (scan<0) return 0;
                    if (buf.accels[scan]<15) return 0;
                    remdv-=buf.dvs[scan];
                    scan--;
                }
            } else {
                if (accel < 5) {
                    return 0;
                }
                buf.accels[i] = accel;
                buf.dvs[i] = dv;
            }
            if (!stage.central && (i == 0 || stages[i-1].numSideParts > 0)) {
                return 0; // or  This stage must mount on a center-only stage.
            }
            massAcc = m_start;
            dvAcc += dv;
        }
        if (height > 7) return 0;
        return dvAcc;
    }

    static boolean verifyHeavyAccel(Configuration configuration, double usefulMass, Buf buf) {
        double massAcc = usefulMass;
        final Stage[] stages = configuration.stages;
        int height = 0;
        final int nstages = configuration.nstages;
        for (int i = 0; i < nstages; i++) {
            final Stage stage = stages[i];
            final double decoupler = i > 0 ? ((stages[i-1].engine.large && stage.fuel.large) ? decouplers[0].mass : decouplers[1].mass) : 0;
            final double massOfEngines = stage.numEngines * stage.engine.mass;
            final int nfueltanks = stage.height * ((stage.central ? 1 : 0) + stage.numSideParts);
            final double m_start = massAcc + decoupler + massOfEngines + nfueltanks * stage.fuel.mfull;
            final double accel = stage.numEngines * stage.engine.thrust / m_start;
            if (i == nstages - 1 && accel < 15) return false;
            final double m_end = massAcc + decoupler + massOfEngines + nfueltanks * stage.fuel.mempty;
            final double isp = (i == nstages - 1)? stage.engine.isp_atm : stage.engine.isp;
            final double dv = Math.log(m_start/m_end) * isp * 9.816;
            height += stage.height;
            if (i == nstages - 1) {
                double remdv = 5000 - dv;
                int scan = i-1;
                while(remdv > 0) {
                    if (scan<0) return true;
                    if (buf.accels[scan]<15) return false;
                    remdv-=buf.dvs[scan];
                    scan--;
                }
            } else {
                buf.accels[i] = accel;
                buf.dvs[i] = dv;
            }
            if (!stage.central && (i == 0 || stages[i-1].numSideParts > 0)) {
                return false;
            }
            massAcc = m_start;
        }
        if (height > 7) return false;
        return true;
    }

    // no need to check base stages
    static double verifyVacuumSolution(Configuration configuration, double usefulMass) {
        double dvAcc = 0;
        double massAcc = usefulMass;
        final Stage[] stages = configuration.stages;
        final int nstages = configuration.nstages;
        int s;
        for (s = 0; s < nstages; s++) {
            final Stage stage = stages[s];
            if (stage.engine.mass != 0) break;
        }
        for (int i = s; i < nstages; i++) {
            final Stage stage = stages[i];
            if (stage.engine.mass == 0) return 0;   // bad solution, zero not on top.
            final double decoupler = i > s ? ((stages[i-1].engine.large && stage.fuel.large) ? decouplers[0].mass : decouplers[1].mass) : 0;
            final double massOfEngines = stage.numEngines * stage.engine.mass;
            final int nfueltanks = stage.height * ((stage.central ? 1 : 0) + stage.numSideParts);
            final double m_start = massAcc + decoupler + massOfEngines + nfueltanks * stage.fuel.mfull;
            final double m_end = massAcc + decoupler + massOfEngines + nfueltanks * stage.fuel.mempty;
            final double isp = stage.engine.isp;
            final double dv = Math.log(m_start/m_end) * isp * 9.816;
            final double accel = stage.numEngines * stage.engine.thrust / m_start;
            if (accel < 5)
                return 0;
            if (!stage.central && (i == s || stages[i-1].numSideParts > 0)) {
                return 0; // or  This stage must mount on a center-only stage.
            }
            massAcc = m_start;
            dvAcc += dv;
        }
        return dvAcc;
    }




    static double calculateHeavyPair(Configuration configuration) {
        double dvAcc = 0;
        double massAcc = 0;
        final Stage[] stages = configuration.stages;
        final int nstages = configuration.nstages;
        for (int i = 0; i < nstages; i++) {
            final Stage stage = stages[i];
            if (stage.engine.mass == 0) return 0;   // bad solution, zero not on top.
            final double decoupler = i > 0 ? ((stages[i-1].engine.large && stage.fuel.large) ? decouplers[0].mass : decouplers[1].mass) : 0;
            final double massOfEngines = stage.numEngines * stage.engine.mass;
            final int nfueltanks = stage.height * ((stage.central ? 1 : 0) + stage.numSideParts);
            final double m_start = massAcc + decoupler + massOfEngines + nfueltanks * stage.fuel.mfull;
            final double m_end = massAcc + decoupler + massOfEngines + nfueltanks * stage.fuel.mempty;
            final double isp = stage.engine.isp;
            final double dv = Math.log(m_start/m_end) * isp * 9.816;
            final double accel = stage.numEngines * stage.engine.thrust / m_start;
            if (accel < 15)
                return 0;
            massAcc = m_start;
            dvAcc += dv;
        }
        return dvAcc;
    }

    static double calculateHeavySolution(Stage stage, double usefulMass) {
        double dvAcc = 0;
        double massAcc = usefulMass;
        if (stage.engine.mass == 0) return 0;   // bad solution, zero not on top.
        final double decoupler = 0;
        final double massOfEngines = stage.numEngines * stage.engine.mass;
        final int nfueltanks = stage.height * ((stage.central ? 1 : 0) + stage.numSideParts);
        final double m_start = massAcc + decoupler + massOfEngines + nfueltanks * stage.fuel.mfull;
        final double m_end = massAcc + decoupler + massOfEngines + nfueltanks * stage.fuel.mempty;
        final double isp = stage.engine.isp;
        final double dv = Math.log(m_start/m_end) * isp * 9.816;
        dvAcc += dv;
        return dvAcc;
    }

    static double calculateMass(Configuration configuration, double usefulMass) {
        double massAcc = usefulMass;
        final Stage[] stages = configuration.stages;
        final int nstages = configuration.nstages;
        int s;
        for (s = 0; s < nstages; s++) {
            final Stage stage = stages[s];
            if (stage.engine.mass != 0) break;
        }
        for (int i = s; i < nstages; i++) {
            final Stage stage = stages[i];
            final double massOfEngines = stage.numEngines * stage.engine.mass;
            final int nfueltanks = stage.height * ((stage.central ? 1 : 0) + stage.numSideParts);
            final double decoupler = i > s ? ((stages[i-1].engine.large && stage.engine.large) ? decouplers[0].mass : decouplers[1].mass) : 0;
            double m_start = massAcc + decoupler + massOfEngines + nfueltanks * stage.fuel.mfull;
            massAcc = m_start;
        }
        return massAcc;
    }

    static ArrayList<Stage> possibleStages = new ArrayList<Stage>(1000000);
    static List<Stage> heavyStages;

    public static void generateAllPossibleStages() {
        for(int i=0; i<fuels.length; i++) {
            for(int j=0; j<engines.length; j++) {
                for(int k=0; k<sides.length; k++) {
                    final int nsides = sides[k];
                    for(int l=1; l<4; l++) {
                        possibleStages.add(new Stage(fuels[i], engines[j], true, nsides, nsides+1, l));
                        if (nsides != 0) {
                            possibleStages.add(new Stage(fuels[i], engines[j], true, nsides, 1, l));
                        }
                        if (nsides > 0) {
                            possibleStages.add(new Stage(fuels[i], engines[j], false, nsides, nsides, l));
                        }
                    }
                }
            }
        }
        Collections.sort(possibleStages, new Comparator<Stage>() {
            @Override
            public int compare(Stage o1, Stage o2) {
                int retval = (int) Math.signum(o2.twr - o1.twr);
                if (retval == 0) {
                    retval = (int) Math.signum(o2.getFullMass() - o1.getFullMass());
                }
                return retval;
            }
        });

//        conf = new Configuration(new Stage[2], 2);
//        for(int i=0; i<heavyStages.size(); i++) {
//            Stage heavyStage = heavyStages.get(i);
//            for(int a=0; a<heavyStages.size(); a++) {
//                conf.stages[0] = heavyStages.get(a);
//                for(int b=0; b<heavyStages.size(); b++) {
//                    conf.stages[1] = heavyStages.get(b);
//                    double thisDV = calculateHeavyPair(conf);
//                    if (thisDV > heavyStage.freedv) {
//                        double thisMass = calculateMass(conf, 2);
//                        Replacement1To2 repl = new Replacement1To2(conf.stages[0], conf.stages[1]);
//                        repl.score = Math.abs(thisMass - heavyStage.height);
//                        if (thisMass > heavyStage.height) {
//
//                            if(heavyStage.worse.size() > 5 && heavyStage.worse.last().score > repl.score || heavyStage.worse.size() <= 5 ) {
//                                heavyStage.worse.add(repl);
//                                if (heavyStage.worse.size() > 20) {
//                                    heavyStage.worse.pollLast();
//                                }
//                            }
//                        } else {
//                            if(heavyStage.better.size() > 5 && heavyStage.better.last().score > repl.score || heavyStage.better.size() <= 5) {
//                                heavyStage.better.add(repl);
//                                if (heavyStage.better.size() > 20) {
//                                    heavyStage.better.pollLast();
//                                }
//                            }
//                        }
//
//                    }
//                }
//            }
//        }

    }


    static ArrayList<Stage> vacuumStages;

    static class VacuumConfiguration {
        Stage i, j, k, l, m;
        double mass;
        double dv;

        VacuumConfiguration(Stage i, Stage j, Stage k, Stage l, Stage m, double mass, double dv) {
            this.i = i;
            this.j = j;
            this.k = k;
            this.l = l;
            this.m = m;
            this.mass = mass;
            this.dv = dv;
        }

        @Override
        public String toString() {
            return "[VACCUM CONFIG: "+
                    (i!= null ? i.toJSON():"")+
                    (j!= null ? j.toJSON():"")+
                    (k!= null ? k.toJSON():"")+
                    (l!= null ? l.toJSON():"")+"]";
        }
    }

    static VacuumConfiguration getAnyVacuumStage(double neededMass, double remainingDV, Predicate<Stage> filterAllStages, int skip1, int skip2) {
        ArrayList<VacuumConfiguration> vacuumConfigurations = new ArrayList<>(9000000);
        vacuumStages = new ArrayList<>(1000000);
        Iterator<Stage> iterator = possibleStages.stream().filter(filterAllStages).iterator();
        while (iterator.hasNext()) {
            Stage nxt = iterator.next();
            if (nxt.canVacuum(neededMass))
                vacuumStages.add(nxt);
        }
        vacuumStages.add(new Stage(zeroFuel, zeroEngine, true, 0, 0, 0));
        vacuumStages.sort(new Comparator<Stage>() {
            @Override
            public int compare(Stage o1, Stage o2) {
                return (int) Math.signum(o1.getFullMass() - o2.getFullMass());
            }
        });
        System.out.println("getAnyVacuumStage^5: "+vacuumStages.size());
        double bestdv = 0;
        Configuration conf = new Configuration(new Stage[5],5);
        long start = System.currentTimeMillis();
        mainloop:
        for(int i=0; i<vacuumStages.size(); i+=skip1) {
            conf.stages[0] = vacuumStages.get(i);
            for(int j=i; j<vacuumStages.size(); j+=skip2) {
                conf.stages[1] = vacuumStages.get(j);
                for(int k=j; k<vacuumStages.size(); k++) {
                    conf.stages[2] = vacuumStages.get(k);
                    for(int l=k; l<vacuumStages.size(); l++) {
                        if (System.currentTimeMillis() - start > 5000)
                            break mainloop;
                        conf.stages[3] = vacuumStages.get(l);
                        for(int m=l; m<vacuumStages.size(); m++) {
                            conf.stages[4] = vacuumStages.get(m);
                            double dv = verifyVacuumSolution(conf, neededMass);
                            double mass = calculateMass(conf, neededMass);
                            if (dv > 0){
                                if (j != 0) {
                                    dv = dv;
                                }
                                if (dv > bestdv) {
                                    bestdv = dv;
                                    System.out.println("Bestdv="+bestdv +"/"+remainingDV+" i="+i+" / "+vacuumStages.size()+" j="+j+" mass="+mass);
                                }
                            }
                            if (dv >= remainingDV) {
                                vacuumConfigurations.add(new VacuumConfiguration(
                                        conf.stages[0].engine.mass == 0 ? null : conf.stages[0],
                                        conf.stages[1].engine.mass == 0 ? null : conf.stages[1],
                                        conf.stages[2].engine.mass == 0 ? null : conf.stages[2],
                                        conf.stages[3].engine.mass == 0 ? null : conf.stages[3],
                                        conf.stages[4].engine.mass == 0 ? null : conf.stages[4],
                                        mass, dv));

                            }
                        }
                    }
                }
            }
        }
        vacuumConfigurations.sort(new Comparator<VacuumConfiguration>() {
            @Override
            public int compare(VacuumConfiguration o1, VacuumConfiguration o2) {
                return (int) Math.signum(o1.mass - o2.mass);
            }
        });
        if (vacuumConfigurations.size() > 0) {
            System.out.println("Checked Preliminary Vacuum Configs Stages (of size 3): " + vacuumConfigurations.size());
            VacuumConfiguration retval = vacuumConfigurations.get(0);
            System.out.println("Found some: mass=" + retval.mass);
            return retval;
        } else {
            System.out.println("Not found simple vacuum configs.. " +"bestdv="+bestdv+" wanted="+remainingDV);
            return null;
        }
    }


    static VacuumConfiguration getLongerVacuumStages2(double neededMass, double neededDV, double maxMass) {
        VacuumConfiguration best = null;
        vacuumStages = new ArrayList<>(1000000);
        final double allowdOwnMass = maxMass - neededMass;
        Iterator<Stage> iterator = possibleStages.stream().filter((stage) -> {
            boolean retval = stage.getFullMass() < allowdOwnMass && stage.canVacuum(neededMass);
            if (stage.engine.code.equals("LV909") && stage.fuel.code.equals("FLT200") && stage.central && stage.numSideParts == 2 && stage.numEngines == 3 && stage.height == 3) {
//                System.out.println("K: "+retval);
            }
            return retval;
        }).iterator();
        while (iterator.hasNext()) {
            vacuumStages.add(iterator.next());
        }
        vacuumStages.add(new Stage(zeroFuel, zeroEngine, true, 0, 0, 0));
        vacuumStages.sort(new Comparator<Stage>() {
            @Override
            public int compare(Stage o1, Stage o2) {
                return (int) Math.signum(o1.getFullMass() - o2.getFullMass());
            }
        });
        Configuration conf = new Configuration(new Stage[5],5);
        long start = System.currentTimeMillis();
        Random random = new Random(0);
        mainloop:
        while(true) {
//            if (System.currentTimeMillis() - start > 60000)
//                break mainloop;
            int i = 0; // random.nextInt(vacuumStages.size());
            conf.stages[0] = vacuumStages.get(i);
            int r;
            int j = 0, k = 0, l = 0, m = 0;
            double massj = 0, massk = 0, massl = 0, massm = 0;
            double heightj = 0, heightk = 0, heightl = 0, heightm = 0;
            for(r=0; r<10; r++) {
                j = 0; // i+random.nextInt(vacuumStages.size()-i);
                conf.stages[1] = vacuumStages.get(j);
                massj = conf.stages[1].getFullMass() + conf.stages[0].getFullMass();
                heightj = conf.stages[1].height + conf.stages[0].height;
                if (massj > allowdOwnMass || heightj > 5); else break;
            }
            if (r == 10) continue;

            for(r=0; r<10; r++) {
                k = random.nextInt(vacuumStages.size());
                conf.stages[2] = vacuumStages.get(k);
                massk = conf.stages[2].getFullMass() + massj;
                heightk = conf.stages[2].height + heightj;
                if (massk > allowdOwnMass || heightk > 5); else break;
            }
            if (r == 10) continue;

            for(r=0; r<10; r++) {
                l = k+random.nextInt(vacuumStages.size()-k);
                conf.stages[3] = vacuumStages.get(vacuumStages.size()-l-1);
                massl = conf.stages[3].getFullMass() + massk;
                heightl = conf.stages[3].height + heightk;
                if (massl > allowdOwnMass || heightl > 5); else break;
            }
            if (r == 10) continue;

            for(r=0; r<10; r++) {
                m = l+random.nextInt(vacuumStages.size()-l);
                conf.stages[4] = vacuumStages.get(vacuumStages.size()-m-1);
                massm = conf.stages[4].getFullMass() + massl;
                heightm = conf.stages[4].height + heightl;
                if (massm > allowdOwnMass || heightm > 5); else break;
            }
            if (r == 10) continue;

            double dv = verifyVacuumSolution(conf, neededMass);
            if (dv >= neededDV ) {
                double thisMass = calculateMass(conf, neededMass);
                if (best == null || best.mass > thisMass) {
                    best = new VacuumConfiguration(
                            conf.stages[0].engine.mass == 0 ? null : conf.stages[0],
                            conf.stages[1].engine.mass == 0 ? null : conf.stages[1],
                            conf.stages[2].engine.mass == 0 ? null : conf.stages[2],
                            conf.stages[3].engine.mass == 0 ? null : conf.stages[3],
                            conf.stages[4].engine.mass == 0 ? null : conf.stages[4],
                            thisMass, dv);
                    System.out.println("Found some best^4: mass="+thisMass+" "+conf.toJSON());
                    if (best.mass < maxMass) {
                        maxMass = best.mass;
                        break;
                    }
                }
            }


        }
        return best;
    }


    static VacuumConfiguration getLongerVacuumStages(double neededMass, double neededDV, double maxMass) {
        VacuumConfiguration best = null;
        vacuumStages = new ArrayList<>(1000000);
        final double allowdOwnMass = maxMass - neededMass;
        Iterator<Stage> iterator = possibleStages.stream().filter((stage) -> {
            boolean retval = stage.getFullMass() < allowdOwnMass && stage.canVacuum(neededMass);
            if (stage.engine.code.equals("LV909") && stage.fuel.code.equals("FLT200") && stage.central && stage.numSideParts == 2 && stage.numEngines == 3 && stage.height == 3) {
//                System.out.println("K: "+retval);
            }
            return retval;
        }).iterator();
        while (iterator.hasNext()) {
            vacuumStages.add(iterator.next());
        }
        vacuumStages.add(new Stage(zeroFuel, zeroEngine, true, 0, 0, 0));
        vacuumStages.sort(new Comparator<Stage>() {
            @Override
            public int compare(Stage o1, Stage o2) {
                return (int) Math.signum(o1.getFullMass() - o2.getFullMass());
            }
        });
        int skips = 0;
        Configuration conf = new Configuration(new Stage[5],5);
        long start = System.currentTimeMillis();
        int li = 1;
        int lj = 1;
mainloop:
        for(int i=0; i<vacuumStages.size(); i++) {
            System.out.println("i="+i);
            conf.stages[0] = vacuumStages.get(i);
            for(int j=i; j<vacuumStages.size(); j+=2) {
                if (System.currentTimeMillis() - start > 60000)
                    break mainloop;
                conf.stages[1] = vacuumStages.get(j);
                double massj = conf.stages[1].getFullMass() + conf.stages[0].getFullMass();
                double heightj = conf.stages[1].height + conf.stages[0].height;
                if (massj > allowdOwnMass || heightj > 5)  {
                    break;
                }
                for(int k=j; k<vacuumStages.size(); k+=1) {
                    conf.stages[2] = vacuumStages.get(k);
                    double massk = massj + conf.stages[2].getFullMass();
                    double heightk = heightj + conf.stages[2].height;
                    if (massk > allowdOwnMass || heightk > 5) {
                        break;
                    }
                    for(int l=k; l<vacuumStages.size(); l+=1) {
                        conf.stages[3] = vacuumStages.get(l);
                        double massl = massk + conf.stages[3].getFullMass();
                        double heightl = heightk + conf.stages[3].height;
                        if (massl > allowdOwnMass || heightk > 5) {
                            break;
                        }
                        for(int m=l; m<vacuumStages.size(); m+=1) {
                            conf.stages[4] = vacuumStages.get(m);
//                            Stage stage = conf.stages[4];
//                            if (stage.engine.code.equals("LV909") && stage.fuel.code.equals("FLT200") && stage.central && stage.numSideParts == 2 && stage.numEngines == 3 && stage.height == 3) {
//                                System.out.println("K: ");
//                            }
                            if (massl + conf.stages[4].getFullMass() > allowdOwnMass || heightl + conf.stages[4].height > 5) {
                                break;
                            }

                            double dv = verifyVacuumSolution(conf, neededMass);
                            if (dv >= neededDV ) {
                                double thisMass = calculateMass(conf, neededMass);
                                if (best == null || best.mass > thisMass) {
                                    best = new VacuumConfiguration(
                                            conf.stages[0].engine.mass == 0 ? null : conf.stages[0],
                                            conf.stages[1].engine.mass == 0 ? null : conf.stages[1],
                                            conf.stages[2].engine.mass == 0 ? null : conf.stages[2],
                                            conf.stages[3].engine.mass == 0 ? null : conf.stages[3],
                                            conf.stages[4].engine.mass == 0 ? null : conf.stages[4],
                                            thisMass, dv);
                                    System.out.println("Found some best^4: mass="+thisMass+" "+conf.toJSON());
                                    if (best.mass < maxMass) {
                                        maxMass = best.mass;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return best;
    }

    private static Configuration findLight1(double lightDistance, double requiredm) {
        Configuration conf = new Configuration(new Stage[1], 1);
        double best = 99999;
        Configuration bestconf = null;
        for(int i=0; i<possibleStages.size(); i++) {
            conf.stages[0]=  possibleStages.get(i);
            double s = verifyVacuumSolution(conf, requiredm);
            double mass = calculateMass(conf, requiredm);
            if (s >= lightDistance && mass <= best) {
                best = mass;
                bestconf = conf.cloneIt();
                //System.out.println("dv="+s+" mass="+ mass +" ["+conf.stages[0].toJSON()+","+conf.stages[1].toJSON()/*+","+conf.stages[2].toJSON()*/+"]");
            }
        }
        return bestconf;
    }

    private static Configuration findLight2(double lightDistance, double requiredm) {
        Configuration conf = new Configuration(new Stage[2], 2);
        double best = 99999;
        Configuration bestconf = null;
        for(int i=0; i<possibleStages.size(); i++) {
            conf.stages[0]=  possibleStages.get(i);
            for(int j=0; j<possibleStages.size(); j++) {
                conf.stages[1]=  possibleStages.get(j);
                if (conf.stages[0].height + conf.stages[1].height  <= 5) {
                    double s = verifyVacuumSolution(conf, requiredm);
                    double mass = calculateMass(conf, requiredm);
                    if (s >= lightDistance && mass <= best) {
                        best = mass;
                        bestconf = conf.cloneIt();
                        //System.out.println("dv="+s+" mass="+ mass +" ["+conf.stages[0].toJSON()+","+conf.stages[1].toJSON()/*+","+conf.stages[2].toJSON()*/+"]");
                    }
                }
                // }
            }
        }
        return bestconf;
    }


    public static Configuration findHeavy2(double requiredm, int lightlen) {
        Configuration conf = new Configuration(new Stage[2], 2);
        double bestmass = 9999;
        Configuration bestconf = null;
        final int maxhlen = 7-lightlen;
        Buf buf = new Buf();
        for(int i=0; i<possibleStages.size(); i++) {
            conf.stages[0]=  possibleStages.get(i);
            System.out.println("I="+i);
            for(int j=0; j<possibleStages.size(); j++) {
                conf.stages[1]=  possibleStages.get(j);
                if (conf.stages[0].height + conf.stages[1].height <= maxhlen) {
                    double s = verifyAnySolution(conf, requiredm, buf);
                    double mass = calculateMass(conf, requiredm);
                    if (s > 0) {
                        if (mass < bestmass) {
                            bestmass = mass;
                            bestconf = conf.cloneIt();
                        }
                    }
                }
            }
        }
        return bestconf;
    }

    static Configuration findHeavy1(double requiredm, int lightlen) {
        Configuration conf = new Configuration(new Stage[1], 1);
        double bestmass = 9999;
        Configuration bestconf = null;
        final int maxhlen = 7-lightlen;
        Buf buf = new Buf();
        for(int i=0; i<possibleStages.size(); i++) {
            conf.stages[0]=  possibleStages.get(i);
            System.out.println("I="+i);
            if (conf.stages[0].height <= maxhlen) {
                double s = verifyAnySolution(conf, requiredm, buf);
                double mass = calculateMass(conf, requiredm);
                if (s > 0) {
                    if (mass < bestmass) {
                        bestmass = mass;
                        bestconf = conf.cloneIt();
                        //System.out.println("dv="+s+" mass="+ mass +" ["+conf.stages[0].toJSON()+","+conf.stages[1].toJSON()+","+conf.stages[2].toJSON()+"]");
                    }
                }
            }
        }
        return bestconf;
    }



    // I = mass=247.875
    // II = mass=177.4
    // III = mass=575.45
    //

    final static int nthreads = 8;

    static class FindHeavy extends Thread {

        int tid;
        private double requiredm3;
        private double requiredm4;
        private int height4;
        double bestmass4= 9999;
        double bestmass3= 9999;
        Configuration bestconf3;
        Configuration bestconf4;

        FindHeavy(int tid, double requiredm3, double requiredm4, int height4) {
            this.tid = tid;
            this.requiredm3 = requiredm3;
            this.requiredm4 = requiredm4;
            this.height4 = height4;
        }

        @Override
        public void run() {
            Configuration conf = new Configuration(new Stage[3], 3);
            final int starti = tid * (possibleStages.size() / nthreads);
            final int endi = Math.min(possibleStages.size(), (1 + tid) * (possibleStages.size() / nthreads));
            Buf buf = new Buf();

            height4 = 7-height4;

            for(int i=starti; i<endi; i++) {
                conf.stages[0]=  possibleStages.get(i);
                for(int j=0; j<possibleStages.size(); j++) {
                    conf.stages[1]=  possibleStages.get(j);
                    for(int k=0; k<possibleStages.size(); k++) {
                        conf.stages[2]=  possibleStages.get(k);
                        if (conf.stages[0].height + conf.stages[1].height + conf.stages[2].height <= 4) {
                            double s = verifyAnySolution(conf, requiredm3, buf);
                            double mass = calculateMass(conf, requiredm3);
                            if (s > 0) {
                                if (mass < bestmass4) {
                                    bestmass4 = mass;
                                    bestconf4 = conf.cloneIt();
                                    System.out.println("dv="+s+" mass="+ mass +" ["+conf.stages[0].toJSON()+","+conf.stages[1].toJSON()+","+conf.stages[2].toJSON()+"]");
                                }
                            }
                            s = verifyAnySolution(conf, requiredm4, buf);
                            mass = calculateMass(conf, requiredm4);
                            if (s > 0) {
                                if (conf.stages[0].height + conf.stages[1].height + conf.stages[2].height <= height4) {
                                    bestmass3 = mass;
                                    bestconf3 = conf.cloneIt();
                                }
                            }
                        }
                    }
                }
            }
        }


    }

    static class FindLightest extends Thread {

        int tid;
        private final double lightDistance;
        private final double requiredm;
        double bestmass4= 1000;
        double bestmass3= 1000;
        Configuration bestconf3;
        Configuration bestconf4;

        FindLightest(int tid, double lightDistance, double requiredm) {
            this.tid = tid;
            this.lightDistance = lightDistance;
            this.requiredm = requiredm;
        }

        public void run() {
            Configuration conf = new Configuration(new Stage[3], 3);
            final int starti = tid * (possibleStages.size() / nthreads);
            final int endi = Math.min(possibleStages.size(), (1 + tid) * (possibleStages.size() / nthreads));

            for(int i=starti; i<endi; i++) {
                conf.stages[0]=  possibleStages.get(i);
                for(int j=0; j<possibleStages.size(); j++) {
                    conf.stages[1]=  possibleStages.get(j);
                    for(int k=0; k<possibleStages.size(); k++) {
                        conf.stages[2]=  possibleStages.get(k);
                        if (conf.stages[0].height + conf.stages[1].height + conf.stages[2].height <= 4) {
                            double s = verifyVacuumSolution(conf, requiredm);
                            double mass = calculateMass(conf, requiredm);
                            if (s >= lightDistance && mass < bestmass4) {
                                bestmass4 = mass;
                                bestconf4 = conf.cloneIt();
                                System.out.println("dv="+s+" mass="+ mass +" ["+conf.stages[0].toJSON()+","+conf.stages[1].toJSON()+","+conf.stages[2].toJSON()+"]");
                            }
                            if (conf.stages[0].height + conf.stages[1].height + conf.stages[2].height <= 3) {
                                if (s >= lightDistance && mass < bestmass3) {
                                    bestmass3 = mass;
                                    bestconf3 = conf.cloneIt();
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    public static void main(String[] args) {
        final double NEEDED_MASS = 11;
        final int NEEDED_DV = 10500;

        generateAllPossibleStages();

        long ts = System.currentTimeMillis();
        FindLightest fls[] = new FindLightest[nthreads];
        for(int i=0; i<nthreads; i++) {
            fls[i] = new FindLightest(i, NEEDED_DV - 5000, NEEDED_MASS);
            fls[i].start();
        }
        for(int i=0; i<nthreads; i++) {
            try {
                fls[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        ts = ts - System.currentTimeMillis();
        System.out.println("Part1 time = "+ts+" msec");


        for (int i1 = 1; i1 < fls.length; i1++) {
            FindLightest fl = fls[i1];
            if (fl.bestmass3 < fls[0].bestmass3) {
                fls[0].bestconf3 = fl.bestconf3;
                fls[0].bestmass3 = fl.bestmass3;
            }
            if (fl.bestmass4 < fls[0].bestmass4) {
                fls[0].bestconf4 = fl.bestconf4;
                fls[0].bestmass4 = fl.bestmass4;
            }
        }

        Configuration light2 = findLight2(NEEDED_DV - 5000, NEEDED_MASS);
        if (light2 != null) {
            int height = light2.getHeight();
            if (height == 3) {
                if (fls[0].bestmass3 > calculateMass(light2, NEEDED_MASS)) {
                    fls[0].bestmass3 = calculateMass(light2, NEEDED_MASS);
                    fls[0].bestconf3 = light2;
                }
            } else{
                if (fls[0].bestmass4 > calculateMass(light2, NEEDED_MASS)) {
                    fls[0].bestmass4 = calculateMass(light2, NEEDED_MASS);
                    fls[0].bestconf4 = light2;
                }
            }
        }

        Configuration light1 = findLight1(NEEDED_DV - 5000, NEEDED_MASS);
        if (light1 != null) {
            int height = light1.getHeight();
            if (height <= 3) {
                if (fls[0].bestmass3 > calculateMass(light1, NEEDED_MASS)) {
                    fls[0].bestmass3 = calculateMass(light1, NEEDED_MASS);
                    fls[0].bestconf3 = light1;
                }
            } else{
                if (fls[0].bestmass4 > calculateMass(light1, NEEDED_MASS)) {
                    fls[0].bestmass4 = calculateMass(light2, NEEDED_MASS);
                    fls[0].bestconf4 = light1;
                }
            }
        }



        ts = System.currentTimeMillis();

        FindHeavy fhs[] = new FindHeavy[nthreads];
        for(int i=0; i<nthreads; i++) {
            fhs[i] = new FindHeavy(i, fls[0].bestmass3, fls[0].bestmass4, fls[0].bestconf4.getHeight());
            fhs[i].start();
        }

        for(int i=0; i<nthreads; i++) {
            try {
                fhs[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        for (int i1 = 1; i1 < fhs.length; i1++) {
            FindHeavy fh = fhs[i1];
            if (fh.bestmass3 < fhs[0].bestmass3) {
                fhs[0].bestconf3 = fh.bestconf3;
                fhs[0].bestmass3 = fh.bestmass3;
            }
            if (fh.bestmass4 < fls[0].bestmass4) {
                fhs[0].bestconf4 = fh.bestconf4;
                fhs[0].bestmass4 = fh.bestmass4;
            }

        }


        ts = ts - System.currentTimeMillis();
        System.out.println("Part2 time = "+ts+" msec");

        ArrayList<Configuration> confs = new ArrayList<>();
        confs.add(new Configuration(fls[0].bestconf3, fhs[0].bestconf4));
        confs.add(new Configuration(fls[0].bestconf4, fhs[0].bestconf3));

        Configuration heavy1 = findHeavy1(fls[0].bestmass3, fls[0].bestconf3.getHeight());
        if (heavy1 != null) {
            confs.add(new Configuration(fls[0].bestconf3, heavy1));
        }
        heavy1 = findHeavy1(fls[0].bestmass4, fls[0].bestconf4.getHeight());
        if (heavy1 != null) {
            confs.add(new Configuration(fls[0].bestconf4, heavy1));
        }
        Configuration heavy2 = findHeavy2(fls[0].bestmass4, fls[0].bestconf4.getHeight());
        if (heavy2 != null) {
            confs.add(new Configuration(fls[0].bestconf4, heavy2));
        }
        heavy2 = findHeavy2(fls[0].bestmass3, fls[0].bestconf3.getHeight());
        if (heavy2 != null) {
            confs.add(new Configuration(fls[0].bestconf3, heavy2));
        }

        double bestHeight = 99999;
        for(int i=0; i<confs.size(); i++) {
            Configuration conf = confs.get(i);
            if (verifyAnySolution(conf, NEEDED_MASS, new Buf()) >= NEEDED_DV) {
                if (bestHeight > calculateMass(conf, NEEDED_MASS)) {
                    verifyAnySolution(conf, NEEDED_MASS, new Buf());
                    System.out.println("Next Best: " + conf.toJSONStringAndInfo(NEEDED_MASS));
                    bestHeight = calculateMass(conf, NEEDED_MASS);
                }
            }
        }

        /*

        VacuumConfiguration someVacuumStage = getAnyVacuumStage(NEEDED_MASS, NEEDED_DV - 5000, (x) -> x.engine.code.equals("LV909") && x.height == 1, 1, 1);
        if (someVacuumStage == null)
            someVacuumStage = getAnyVacuumStage(NEEDED_MASS, NEEDED_DV - 5000, (x) -> x.engine.code.equals("LVN") && x.height == 1, 1, 1);
        if (someVacuumStage == null) {
            someVacuumStage = getAnyVacuumStage(NEEDED_MASS, NEEDED_DV - 5000, (x) -> true, 3, 5);
        }
        double vacuumMass = someVacuumStage != null ? someVacuumStage.mass : maxPossibleLightWeight;
        VacuumConfiguration maybeBetter = getLongerVacuumStages(NEEDED_MASS, NEEDED_DV - 5000, Math.min(maxPossibleLightWeight, vacuumMass));
        if (maybeBetter != null && maybeBetter.mass < someVacuumStage.mass) {
            someVacuumStage = maybeBetter;
        }
        ArrayList<HeavyConfiguration> heavyConfigurations = calculateHeavyConfigs(someVacuumStage.mass);
        Configuration bestConf = null;
        final Stage[] stages = new Stage[100];
    mainloop:

        for(int i=0; i<heavyConfigurations.size(); i++) {
            HeavyConfiguration heavyConfiguration = heavyConfigurations.get(i);
            VacuumConfiguration vacuumConfiguration = someVacuumStage;
            int dest = 0;
            if (vacuumConfiguration.i != null) stages[dest++] = vacuumConfiguration.i;
            if (vacuumConfiguration.j != null) stages[dest++] = vacuumConfiguration.j;
            if (vacuumConfiguration.k != null) stages[dest++] = vacuumConfiguration.k;
            if (vacuumConfiguration.l != null) stages[dest++] = vacuumConfiguration.l;
            if (vacuumConfiguration.m != null) stages[dest++] = vacuumConfiguration.m;
            stages[dest++]= heavyConfiguration.i;
            stages[dest++]= heavyConfiguration.j;

            Configuration conf = new Configuration(stages, dest);
            double result = verifyAnySolution(conf, NEEDED_MASS, new Buf());
            if (result >= NEEDED_DV) {
                conf.mass = calculateMass(conf, NEEDED_MASS);
                if (bestConf == null || bestConf.mass > conf.mass) {
                    bestConf = conf.cloneIt();
                    System.out.println("Next best? result=" + result + " mass=" + conf.mass + " value=" + conf.toJSON());
                    verifyAnySolution(conf, NEEDED_MASS, new Buf());
                }
            }
        }

        if (bestConf != null) {
            System.out.println("Best("+calculateMass(bestConf, NEEDED_MASS)+")="+bestConf.toJSON());
        } else {
            System.out.println("NO Best");
        }

        */


        //findSolution(10.4, 7000);
//        Configuration conf = new Configuration(new Stage[] {
//                new Stage(getFuel("FLT400"), getEngine("LVT30"), true, 0, 1, 1),
//                new Stage(getFuel("FLT400"), getEngine("LV909"), false, 2, 2, 1),
//                new Stage(getFuel("X20032"), getEngine("SKIPPER"), true, 2, 3, 1),
//                new Stage(getFuel("X20032"), getEngine("MAINSAIL"), true, 6, 7, 2),
//        });
//        boolean ok = calculate(conf, 10, 6000);
//        double mass = calculateMass(conf, 10);
//        System.out.println("Mass="+mass+" ok="+ok);
    }

}
